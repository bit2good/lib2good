﻿namespace Lib2Good
{
  using System;
  using System.Collections.Generic;

  /// <summary>
  /// Static event manager.
  /// </summary>
  /// <author>Sebastian Erben</author>
  public static class Events
  {
    private static Dictionary<string, Action> events = new Dictionary<string, Action>();


    /// <summary>
    /// Returns an array of all actions attached to an event.
    /// </summary>
    /// <param name="eventName">The name of the event.</param>
    /// <returns>All actions attached to the event as an array of Delegates.</returns>
    public static Delegate[] Actions(string eventName)
    {
      var tempEvent = GetEventByName(eventName);

      return tempEvent.GetInvocationList();
    }

    /// <summary>
    /// Attaches an action to an event.
    /// </summary>
    /// <param name="eventName">The name of the event to attach the action to.</param>
    /// <param name="action">The action to attach to the event.</param>
    public static void Attach(string eventName, Action action)
    {
      var tempEvent = GetEventByName(eventName);

      tempEvent += action;
      events[eventName] = tempEvent;
    }

    /// <summary>
    /// Clears all events.
    /// </summary>
    public static void Clear() {
      events.Clear();
    }

    /// <summary>
    /// Clears a specific event.
    /// </summary>
    /// <param name="eventName">Name of the event to clear.</param>
    public static void Clear(string eventName) {
      if (events.ContainsKey(eventName)) {
        events[eventName] = null;
      }
    }

    /// <summary>
    /// Returns the count of events.
    /// </summary>
    /// <returns>The count of events.</returns>
    public static int Count()
    {
      return events.Count;
    }

    /// <summary>
    /// Returns the count of actions attached to an event.
    /// </summary>
    /// <param name="eventName">The name of the event.</param>
    /// <returns>The count of actions attached to the event.</returns>
    public static int Count(string eventName)
    {
      var tempEvent = GetEventByName(eventName);

      return tempEvent != null ? tempEvent.GetInvocationList().Length : 0;
    }

    /// <summary>
    /// Detaches an action from an event.
    /// </summary>
    /// <param name="eventName">The name of the event to detach the action from.</param>
    /// <param name="action">The action to detach from the event.</param>
    public static void Detach(string eventName, Action action)
    {
      var tempEvent = GetEventByName(eventName);

      tempEvent -= action;
      events[eventName] = tempEvent;
    }

    /// <summary>
    /// Returns if an event exists.
    /// </summary>
    /// <param name="eventName">The name of the event.</param>
    /// <returns>true if the event exists, false otherwise.</returns>
    public static bool Exists(string eventName)
    {
      return events.ContainsKey(eventName);
    }

    /// <summary>
    /// Lists the names of all events.
    /// </summary>
    /// <returns>All event names as string array.</returns>
    public static string[] List()
    {
      var result = new String[events.Count];
      events.Keys.CopyTo(result, 0);

      return result;
    }

    /// <summary>
    /// Trigger an event and call all attached actions.
    /// </summary>
    /// <param name="eventName">The name of the event to trigger.</param>
    public static void Trigger(string eventName)
    {
      var tempEvent = GetEventByName(eventName);

      if (tempEvent != null) {
        tempEvent.Invoke();
      }
    }


    private static Action GetEventByName(string eventName)
    {
      if (!events.ContainsKey(eventName))
      {
        events.Add(eventName, null);
      }

      return events[eventName];
    }
  }
}
