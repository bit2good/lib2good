﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System;


public class EventsTest
{
  [Test]
  public void ActionsTest()
  {
    Lib2Good.Events.Clear();

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);

    var testDelegates = Lib2Good.Events.Actions("testAction");
    Assert.AreEqual(testAction, testDelegates[0] as Action);

    var notTestAction = new Action(() => { Debug.Log("test"); });
    Assert.AreNotEqual(notTestAction, testDelegates[0] as Action);
  }

  [Test]
  public void AttachTest()
  {
    Lib2Good.Events.Clear();

    Assert.AreEqual(0, Lib2Good.Events.Count("testAction"));

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);

    Assert.AreEqual(1, Lib2Good.Events.Count("testAction"));
  }

  [Test]
  public void ClearTest()
  {
    Lib2Good.Events.Clear();

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);
    Assert.AreEqual(1, Lib2Good.Events.Count("testAction"));

    Lib2Good.Events.Clear();
    Assert.AreEqual(0, Lib2Good.Events.List().Length);
  }

  [Test]
  public void ClearEventTest()
  {
    Lib2Good.Events.Clear();

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);
    Assert.AreEqual(1, Lib2Good.Events.Count("testAction"));

    Lib2Good.Events.Clear("testAction");
    Assert.AreEqual(0, Lib2Good.Events.Count("testAction"));
  }

  [Test]
  public void CountTest()
  {
    Lib2Good.Events.Clear();
    Assert.AreEqual(0, Lib2Good.Events.Count());

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);

    Assert.AreEqual(1, Lib2Good.Events.Count());
  }

  [Test]
  public void CountEventTest()
  {
    Lib2Good.Events.Clear();
    Assert.AreEqual(0, Lib2Good.Events.Count("testAction"));

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);

    Assert.AreEqual(1, Lib2Good.Events.Count("testAction"));
  }

  [Test]
  public void DetachTest()
  {
    Lib2Good.Events.Clear();

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);
    Lib2Good.Events.Attach("testAction", testAction);

    Assert.AreEqual(2, Lib2Good.Events.Count("testAction"));

    Lib2Good.Events.Detach("testAction", testAction);

    Assert.AreEqual(1, Lib2Good.Events.Count("testAction"));
  }

  [Test]
  public void ExistsTest()
  {
    Lib2Good.Events.Clear();
    Assert.AreEqual(false, Lib2Good.Events.Exists("testAction"));

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);

    Assert.AreEqual(true, Lib2Good.Events.Exists("testAction"));
  }

  [Test]
  public void ListTest()
  {
    Lib2Good.Events.Clear();

    Assert.AreEqual(0, Lib2Good.Events.List().Length);

    var testAction = new Action(() => { });
    Lib2Good.Events.Attach("testAction", testAction);
    Assert.AreEqual(1, Lib2Good.Events.List().Length);

    Lib2Good.Events.Attach("nextTestAction", testAction);
    Assert.AreEqual(2, Lib2Good.Events.List().Length);
  }

  [Test]
  public void TriggerTest()
  {
    Lib2Good.Events.Clear();

    var actionCalled = 0;

    var testAction = new Action(() => { actionCalled++; });
    Lib2Good.Events.Attach("testAction", testAction);

    Lib2Good.Events.Trigger("testAction");
    Assert.AreEqual(1, actionCalled);

    Lib2Good.Events.Trigger("testAction");
    Assert.AreEqual(2, actionCalled);
  }
}
